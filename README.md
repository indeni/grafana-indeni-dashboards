# Grafana Dashboards 

This repository contains sample dashboards that will display performance and health metrics obtained from the Indeni platform. See [Indeni documentation](https://indeni.com/docs/user-guide/part-9-grafana-dashboards/9-1-configuring-grafana/) for installing and configuring these dashboards. 

## What dashboards are provided? 
* Check Point Secure Gateway device dashboard
* Palo Alto Networks firewall device dashboard
* Composite dashboard for multiple devices (coming soon)

## About these dashboards 
These dashboards assume a datasource named Indeni-APIv2. They can be imported from the Grafana webUI. See [Indeni documentation](https://indeni.com/docs/user-guide/part-9-grafana-dashboards/9-2-importing-dashboards/) for importing instructions. 

These dashboards should work out-of-the-box. Although Indeni does not provide support for these dashboards in your environment, you are more than welcome to leverage them and customize them to meet your needs. 

## Who to talk to? 
 If you would like specific metrics to be added, or you have ideas to improve these dashboards, or you want to contribute, drop us an email (product@indeni.com).

